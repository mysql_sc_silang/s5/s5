=======================================
	MYSQL- CULMINATING ACTIVITY
=======================================

-- 1. Return the customerName of the customers who are from the Philippines
SELECT customerName FROM customers WHERE country = "Philippines";


-- 2. Return the contactLastName and contactFirstName of customers with name "La Rochelle Gifts"
SELECT contactLastName, contactFirstName FROM customers WHERE customerName = "La Rochelle Gifts";


-- 3. Return the productName and MSRP of the product named "The Titanic"
SELECT productName, MSRP FROM products WHERE productName = "The Titanic";


-- 4. Return the firstName and lastName of the employee whose email is "jfirrelli@classicmodelcars.com"
SELECT firstName, lastName FROM employees WHERE email = "jfirrelli@classicmodelcars.com";


-- 5. Return the names of customers who have no registered state
SELECT customerName FROM customers WHERE state IS NULL;


-- 6. Return the firstName, lastName, email of the employee whose last name is Patterson and first name is Steve
SELECT firstName, lastName, email FROM employees WHERE lastName = "Patterson" AND firstName = "Steve";


-- 7. Return customerName, country, and creditLimit of customers whose countries are NOT USA and whose credit limits are greater than 3000
SELECT customerName, country, creditLimit FROM customers WHERE country != "USA" AND creditLimit > 3000;


-- 8. Return the customer numbers of orders whose comments contain the string 'DHL'
SELECT orders.customerNumber FROM orders WHERE orders.comments LIKE "%DHL%";


-- 9. Return the product lines whose text description mentions the phrase 'state of the art'
SELECT productLine FROM productLines WHERE textDescription LIKE "%state of the art%";


-- 10. Return the countries of customers without duplication
SELECT DISTINCT country FROM customers; 


-- 11. Return the statuses of orders without duplication
SELECT DISTINCT status FROM orders;


-- 12. Return the customer names and countries of customers whose country is USA, France, or Canada
SELECT customerName, country FROM customers WHERE country IN ("USA","France","Canada");


-- 13. Return the first name, last name, and office's city of employees whose offices are in Tokyo
SELECT employees.firstName, employees.lastName, offices.city FROM offices
	JOIN employees ON offices.officeCode = employees.officeCode
	WHERE offices.city = "Tokyo";

-- 14. Return the customer names of customers who were served by the employee named "Leslie Thompson"
SELECT customers.customerName FROM employees
	JOIN customers ON employees.employeeNumber = customers.salesRepEmployeeNumber
	WHERE employees.lastName = "Thompson" AND employees.firstName = "Leslie";


-- 15. Return the product names and customer name of products ordered by "Baane Mini Imports"
SELECT products.productName FROM customers
	JOIN orders ON customers.customerNumber = orders.customerNumber
	JOIN orderdetails ON orders.orderNumber = orderdetails.orderNumber
	JOIN products ON orderdetails.productCode = products.productCode
	WHERE customers.customerName = "Baane Mini Imports";


-- 16. Return the employees' first names, employees' last names, customers' names, and offices' countries of transactions whose customers and offices are in the same country
SELECT employees.firstName, employees.lastName, customers.customerName, offices.country FROM employees
	JOIN customers ON employees.employeeNumber = customers.salesRepEmployeeNumber
	JOIN offices ON employees.officeCode = offices.officeCode
	WHERE customers.country = offices.country;


-- 17. Return the product name and quantity in stock of products that belong to the product line "planes" with stock quantities less than 1000
SELECT productName, quantityInStock FROM products WHERE productLine = "planes" AND quantityInStock < 1000;


-- 18. Show the customer's name with a phone number containing "+81".
SELECT customerName, phone FROM customers WHERE phone LIKE "+81%";


-- 19. Find all customers from US
SELECT * FROM customers WHERE country LIKE "US%";


-- 20. Show the full details of a customer named La Rochelle Gifts.
SELECT * FROM customers WHERE customerName = "La Rochelle Gifts";


=======================================
	MYSQL- STRETCH GOALS
=======================================

-- 1. Return the customer names of customers whose customer names don't have 'a' in them
SELECT customerName FROM customers WHERE customerName NOT LIKE "%a%";


-- 2. Return the last names and first names of employees being supervised by "Anthony Bow"
SELECT firstName, lastName FROM employees WHERE reportsTo = 1143;


-- 3. Return the product name and MSRP of the product with the highest MSRP
SELECT productName, MSRP FROM products WHERE
MSRP=(SELECT MAX(MSRP) FROM products);

-- 4. Return the number of customers in the UK
SELECT COUNT(customerName) FROM customers WHERE country = "UK";


-- 5. Return the number of products per product line



-- 6. Return the number of customers served by every employee



-- 7. Show the customer's name with the highest credit limit.
SELECT customerName, creditLimit FROM customers WHERE
creditLimit=(SELECT MAX(creditLimit) FROM customers);